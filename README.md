# IndividualAssignment2

## Overview
This project demonstrates the Continuous Delivery (CD) process for a simple web service implemented in Rust. It encompasses a straightforward Rust application, containerization through Docker, and a CI/CD pipeline for automating the build, test, and deployment phases.


### Clone the Repository and Edit the files

```bash
git clone https://gitlab.com/IsaacWang1117/week4miniproject.git
```

### Build and Run the Service Locally
Inside the project directory, build the project using Cargo to ensure it compiles successfully

```bash
cargo build

cargo run
```

### Test the Microservice
Verify that the microservice is functioning correctly by sending a request to the running application

```bash
curl http://localhost:3000/fruit
```

### Build the Docker Image
With Docker Desktop running, build the Docker image from the Dockerfile
```bash
docker build -t myimage .
```
![Docker image built](./rust-axum-random-fruit-microservice/docker_image_built.png)

### Run the Docker Container

Run the Docker container from the image

```bash
docker run -dp 3000:3000 myimage
```

### Test the Docker Container

![Test](./rust-axum-random-fruit-microservice/docker_test_1.png)
![Test](./rust-axum-random-fruit-microservice/docker_test_2.png)


### CI/CD Pipeline
The CI/CD pipeline, defined in `.gitlab-ci.yml`, consists of several stages including build, test, and deploy. These stages ensure that the code is automatically tested and deployed to the specified environment upon each commit, following best practices for continuous integration and delivery.

1. Build: Compiles the Rust project and builds the Docker image.
2. Test: Runs automated tests against the compiled project.
3. Deploy: Deploys the Docker image to a development, staging, or production environment, based on the branch and conditions specified in .gitlab-ci.yml.

### Using docker-compose.yml
The `docker-compose.yml` file specifies how the Docker containers should be built and run. It defines services, networks, and volumes needed for the application, allowing for a single command deployment and management of the service for development or testing.